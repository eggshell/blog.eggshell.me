+++
title = "State of the Kubernetes Service Mesh (July 2019)"
tags = ["Kubernetes", "ServiceMesh", "Containers", "Linux"]
date = "2019-06-12"
+++

Recently, I was asked about service meshes in the context of Kubernetes.
Admittedly, this is an area I lack knowledge in as the need to run applications
in a meshed Kubernetes environment typically arises when you are running a
large distributed system instead of demoware. I had some (mostly negative)
experiences fiddling with Istio last year, and haven't touched the subject
since.

Unable to answer the questions around service meshes, I decided to take my
opportunity of attending the 2019 KubeCon EMEA conference and spin that into
learning as much about the current Kubernetes service mesh landscape as I could.
I came back with more information than is probably necessary, and all of the
speakers I saw were amazing, so I will try to boil down this newfound knowledge
as best I can.

## Table of Contents

1. Service Mesh Primer
    * What is a Service Mesh?
    * Control Plane vs. Data Plane
1. The Current Landscape
    * Istio
    * Linkerd
    * Build Your Own
    * Others
1. Footnotes

## Service Mesh Primer

### What is a Service Mesh?

The first thing you might ask yourself when delving into the service mesh world
is: "What even is a service mesh?". That is a great question, and one that can
actually be answered fairly succinctly. In short, a service mesh can be thought
of as a layer 7[^1] network overlay, separating the network architecture of your
distributed application from the infrastructure it runs on. This allows for
standardization of certain features in the "cloud native"[^2] development
landscape we now find ourselves in with the advent of Kubernetes. Examples of
these standardized features in a service mesh usually include:

* Resiliency in the form of retries or timeouts
* Layer 7 load balancing
* Service Discovery
* Traffic Management via request routing
* "Circuit breaking", or failover prevention
* Service Authentication usually via mTLS
* Metrics

With distributed applications running on "legacy"[^3] infrastructure, the network
is treated as a pipe that just moves packets around without caring about what
those packets actually contain. Extra features like service discovery would need
to come from an additional tool, for instance Hashicorp's Consul, and be deployed
with the rest of your distributed application's stack, which results in a lot of
overhead.

Now, imagine you are moving a distributed application to legacy infrastructure to
containerized infrastructure. You've poured through your application's codebase,
built some container images, written up a Helm chart, and gotten the base
application deployed onto your Kubernetes cluster. Sure, the Kubernetes API
gets you a few things like layer 4 load-balancing and service discovery, but
what about those other nifty things listed above? Well, you have two choices:

1. Bolt on a proxy, desired set of applications, and any applicable application
   logic to your Helm chart.
2. Grab a service mesh off-the-shelf.

The second option is basically an out-of-the-box approach to the first. Why go
through the trouble of writing all of this when there are a myriad of packaged
solutions which tackle these problems? If Kubernetes is an infrastructure
abstraction layer, then a service mesh can be thought of as a network abstraction
layer. The goal is to move these features up the stack to the application layer,
allowing for the decoupling of business logic from infrastructure/network logic
when running distributed applications in this "meshed" network. This will[^4]
enhance portability across cloud providers and make multi-cloud application
deployments, with all the accoutrements your ops team could ask for, a more
obtainable goal.

### Control Plane vs. Data Plane

Now that we know what a service mesh gets us and what its general feature set is,
it's time to break down the major distinction between the two areas of a service
mesh: the control plane and the data plane. In general networking theory, the
control plane makes decisions about where traffic is sent while the data plane
forwards traffic to the next hop according to logic defined by the control plane.
In other words, the control plane draws your network topology or routing table
at the edge while the data plane actually handles moving inbound packets around
to their destination inside the network. This would typically be accomplished
by your router architecture.

The above also holds true for a service mesh, it is just accomplished in a way
that more easily drops in to the containerized infrastructures of today. The data
plane of a service mesh is made up of sidecar proxies, which are containers
spun up inside of your Kubernetes pods alongside the containers you define for
those pods. All traffic for the containers you define first runs through their
associated sidecar proxies, where traffic introspection can take place, which
gets you the features listed in the previous section.

Now that we have proxies on all of our pods inside our distributed application,
we need a way to manage them. That is where the control plane comes in and
defines all of the policy and configuration for the desired feature set of the
sidecar proxies. This usually manifests in a UI (whether it be a web portal or
CLI) where humans can define what they want their service mesh to accomplish.

That's it! You now know enough about service meshes to be dangerous at a high
level. There are many resources out there which go deeper on this specific
topic, but it is important to understand these concepts when comparing different
service meshes, as they currently all follow this general approach.

## The Current Landscape

So let's talk about the current state of affairs with Kubernetes service meshes
in the industry. Layer5 has created a giant list[^5] of current projects in this
space, so I will cover some of the entries from the **Functional** section of
that list.

## Istio

At the time of this writing, Istio is most certainly the de facto choice for
Kubernetes service meshes. This is due to a confluence of factors which we will
dive into below. However, that is not to say that Istio is inherently bad or
should be avoided, but I think it is important to take a step back and read the
temperature of the room before we as a community put all of our eggs into one
basket with this many implications.

### Why Istio is the Front Runner

Istio's current dominance in the industry category in which it resides was
brought about by three main factors:

#### 1. Being Early to the Party

You've heard of fashionably late, but have you heard of fantastically early?
Sure, Istio did not officially release until July of 2018, but it had a long
road to travel before hitting the coveted 1.0 release. Announced in May of 2017,
Istio began its life in grand fashion as the first "from the ground up"
Kubernetes service mesh, designed to be run in fancy new container orchestrated
environments and hitting every buzzword possibly imaginable in this space.

The project was able to capitalize on this feature-touting early on, generating
a ton of great press in its nacent stage and attracting great community talent
from both open-source and corporate circles. Having the hottest sub-project of
the hottest distributed systems project has obviously has its perks, and Istio
was able to synonimize their project's name with what it aims to accomplish a la
Xerox and Post-It Notes.

Speaking of which, that brings us to our next topic.

#### 2. Corporate and Community Backing

Speaking of perks, Istio has the huge benefit of garnering the eye of both large
companies and open source developers. The product of an initial joint venture
between Google, IBM and Lyft, the project immediately ballooned in popularity
as these companies committed both their resources and people to improving the
project. The scope of support that these companies are able to provide lends
immense credibility to any open source project, which theoretically attracts
open source contributors as plenty of these people want to work on projects which
have legitimate longevity.

However, this is absolutely not a bad thing. The "big kids" on the block have
not only the people and technical resources these kinds of projects require, but
also the money. Companies like Google, IBM, VMware, Canonical (etcetera) are
able to spend big on conference sponsorships and public enablement to help
adoption of these new technologies. The fact that Google and IBM can roll the
Istio community up into the Kubernetes community as a whole provides a lot of
value to both themselves and community members, creating a very cohesive software
community and, hopefully, a very cohesive resulting product.

#### 3. Targeted Adoption

The process of dogfooding as just described is not only limited to fiduciary
duties, but is also extended to technical decisions. Istio is already being
adopted into other projects started by Google, as Knative ships with an Istio
install out of the box. While it is great that consistency is being stressed
longitudinally along the stack, the fact that one company created and oversees
the most popular container orchestration engine, default service mesh for that
orchestration engine and the shiny new toy that is FaaS on tjat orchestration
engine is slightly concerning.

This vertical integration in Cloud Native technologies would be all well and
good in a perfect world where all software is finished and everything works
flawlessly. In reality, there are some legitimate concerns with this
monolithic approach.

### Istio's Setbacks

Where to begin? First of all, I want to state upfront that there are a ton of
people working very hard on this project that make it do wildly impressive
things. Everything in the Cloud Native space is experimental with regards to
real-world use and serious adoption by ISVs and other third parties. There are
hurdles to overcome with a completely new and different application deployment
model, both technological and people-oriented.

#### This Stuff is Hard

None of what we are talking about here is easy or trivial to accomplish. Istio
suffers from what many, including myself, call "The OpenStack Problem". This is
where a project seemingly springs up from the ether, garners a large amount of
attention, makes great claims as to what it will set out to accomplish, then
inevitably falls short of those claims. It is difficult to successfully onboard
and ramp up people into an open source project, and that is further complicated
by the staggeringly large amount of people attracted to this project.

This leads to a large amount of developers trying to get their feet wet with
low hanging fruit with not enough mentorship to go around, resulting in a
shortage of people familiar enough with the codebase and overall architecture
to make real changes. This also stifles outside voices and puts some folks off
from contributing to the project at all, leaving the initial braintrust and
maybe a few newcomers in charge, meaning real changes don't happen.

This would be fine if all software worked perfectly, but no software works
perfectly.

#### Latency

As an example of "hard stuff that necessities real change", let's talk about
request latency in Istio. As we discussed in the primer section, a proxy gets
bolted onto each pod in a Kubernetes deployment and all traffic for those pods
goes through the proxy. This inherently introduces some amount of latency and
this latency is present in any service mesh following this proxy design pattern.

The problem is the Istio's proxy, Envoy, is leveraged for everything *and also*
has to go through Istio Security's internal CA. So for every request flowing
into and out of a pod, Istio has to encrypt the message, send packets, decrypt
and verify against the CA for *every single microservice communication in a
Kubernetes cluster*. Now, not only are you worried about north-south latency,
you also have to contend with east-west latency inside your network, which is
a fault tolerance not commonly built into software up to this point (at least
at the scale of request latency we are talking about here).

#### Scope

The final prong holding Istio back is the classic problem of scope in a software
project. Istio was originally intended to extend Kubernetes and provide the
"standard" set of features expected of a modern service mesh. This is the list
of key features the project aimed to provide at the time it was first announced
in 2017[^6]:

* Automatic zone-aware load balancing and failover for HTTP/1.1, HTTP/2, gRPC, and TCP traffic.
* Fine-grained control of traffic behavior with rich routing rules, fault tolerance, and fault injection.
* A pluggable policy layer and configuration API supporting access controls, rate limits and quotas.
* Automatic metrics, logs and traces for all traffic within a cluster, including cluster ingress and egress.
* Secure service-to-service authentication with strong identity assertions between services in a cluster.

And this is the list of key features from Istio's current "What is Istio?"
page[^7]:

**To-Do** rethink the below list

* Traffic Management
    * Circuit Breakers
    * Timeouts
    * Retries
    * A/B Testing
    * Canary Rollouts
    * Staged Rollouts
    * Service Discovery
    * TLS Termination
    * Fault Injection
* Security
    * Authentication and Authorization
    * Policy Enforcement
    * Pod-to-Pod and Service-to-Service communication security at layer 3 and 7.
* Observability
    * Distributed Tracing
    * Monitoring
    * Logging
    * Custom Dashboards
* Galley
    * Configuration Validation
    * Intended to insulate the rest of Istio from the underlying platform (Kubernetes)

As we can see, the project has ballooned in size in a short amount of time,
namely in the spaces of Continuous Deployment and Configuration Management. What
was once a nice set of bells and whistles for a Kubernetes cluster is morphing
into an abstraction layer on top of Kubernetes, forming an amorphous distributed
systems sandwich with extreme complexity and feature-creep. This identity problem
results in two major pieces of fallout:

* "Too Many Cooks" - so many developers in the room bringing new features to
the table that could be their own projects or as pluggable parts of Istio.

* The "everything AND the kitchen sink" approach essentially sells itself to
enterprises. They have the resources to train people on this complexity, but
in practice in a lot of places, Istio will just get dropped in to a cluster
and be expected to "just work".

These two problems contribute to the ever-present and ever-pervasive questions
of "What is Istio?" and "Why is Istio not working for my organization?", which
might be what brought you to this post in the first place.

### Istio Conclusion

While Istio is the clear front runner in the community, it has issues that need
to be addressed and technical hurdles which need to be solved. I am not trying
to bash the developers or the project itself, but rather provide a clear
understanding of why there seems to be such a problem of identity and
functionality within the project.

So, what else is out there?

## Linkerd

The other heavy hitter currently in this space is Linkerd. Announced in Spring
2016 and hitting 1.0 in Spring 2017, Linkerd has been around a little longer and
definitely carries some baggage because of that. Linkerd was conceived at a time
when Kubernetes was not considered the default container orchestration platform,
offering support for AWS ECS, Mesosphere's DC/OS and Docker Swarm.

Additionally, Linkerd was a popular choice for hybrid infrastructure where virtual
machines and/or baremetal are involved, which made it a popular choice for
users who wanted to more smoothly transition to containerized workloads in
production. As it can be deployed on a per-host basis, Linkerd 1.x is able to
integrate with environments that don't support the sidecar deployments of
Istio or Linkerd 2.0. Linkerd 1.x also does not support TCP connections between
services' sidecar proxies.

### Linkerd 2.0

To shed technical debt and reform its image as a "from the ground up" Kubernetes
service mesh, Linkerd announced the 1.0 release of Linkerd 2.0 in September 2018
[^8]. Offering a one-liner install and sane configuration defaults out of the box,
Linkerd saw the complexity of its main competitor and aimed to alleviate these
hassles at the cost of some advanced features available in 1.x such as
routing and circuit-breaking. This clear delineation between versions also
allowed for a rewrite in Golang, the obvious choice as Kubernetes and Istio are
also both written in Golang.

Compared to its predecessor, Linkerd 2.x is limited to Kubernetes. This cuts off
some users from migrating to 2.x, but greatly enhances the usability for k8s-only
deployments. It does offer some Continuous Deployment features, but does not
seem to put as much emphasis on this as Istio does.

### The Advantage of Linkerd

Besides having a clearer focus and more limited feature set, Linkerd clearly
wins the latency and resource consumption battles when pitted against Istio.
As recently as May 2019, both of these service meshes were benchmarked by
Kinvolk[^9] in an attempt to compare costs of adopting either solution[^10]. If
you are deeply interested in these sorts of comparisons, you should read the
entire Kinvolk writeup, but I will do my best to provide the TL;DR here.

With a consistent synthetic load of 600 requests per second over a period of
30 minutes[^11] here are the results:

![latency_benchmark](https://kinvolk.io/media/600rps-latency.png)

Here we can see that Istio is generating latencies in the minutes range, with
a 3.6% of requests getting dropped or erroring out. At this load, Istio would
need to scale out before reaching this point, but that is again more configuration
and complexity put onto the user from the self-described "out of the box" service mesh.

Linkerd, on the other hand, is not overloaded and has request latencies of about
1 second (and change). While this is not as fast as if you were running in a
non-meshed environment, it handily outperforms its main competitor and stays in
the range of acceptable latency while giving you its full feature set at the
upper limit of performance.

**Possibly include cpu/mem benchmarks, but this is really stretching length
as is. Thoughts?**

### Linkerd Conclusion

For out of the box approaches available today, Linkerd 2.x clearly shows that it
has the technical chops to make a Kubernetes service mesh succeed with minimal
user configuration. It might not tout itself to be the end-all be-all of your
containerized workflow, but it extends Kubernetes cleanly and has a clearly
defined purpose.

## Keep Your Eyes Open

As with all technology, there are tons of other companies and projects out there
trying to achieve the same goals. Again pointing to Layer5's breakdown of the
service mesh landscape[^5], we can see that there are other entries in the
"Functional" section which we have not talked about today. However, there are
serious feature concerns with some of these offerings such as Kong's lack of
Auto Proxy Injection or Mesher's lack of microservice communication over
TCP and/or WebSockets.

The aim of this post was to compare the major leaders out there today, and indeed
I did not see any talks at KubeCon EMEA 2019 that centered around any of these
lesser-known names. Perhaps a follow-up post will be in order where I try to
play around with some of them and record my experiences, but that is a job for
another day.

## Conclusion

Perhaps naively, I went into writing this post with the intention of being able
to provide a clear-cut answer to the question: "Which service mesh should I use?".
The answer I can provide is: "It depends."

If you want to be a part of the Istio community and toe the line of loyalty to
the crowned leader of Kubernetes service meshes, then absolutely use Istio. It
has some real benefits and very strong promises for what it intends to deliver,
and it needs all the development help it can get if it aims to actually achieve
those goals.

If you care more about raw numbers and want your ops team to be happier, then
Linkerd 2.x is the clear choice if you are running in Kubernetes only. If you
have other environments, whether they be traditional infrastructure or a legacy
container orchestration platform, then Linkerd 1.x is pretty much the only
game in town.

What are your thoughts on these findings, and what do you hope to see out of the
service mesh community in both the near and far-flung future? Let us know!

## Footnotes

[^1]: The application layer in the OSI model.
[^2]: Translation: very layer 7-focused.
[^3]: Here meaning virtual machines and baremetal.
[^4]: In theory, that is.
[^5]: https://layer5.io/landscape/
[^6]: https://developer.ibm.com/dwblog/2017/istio/
[^7]: https://istio.io/docs/concepts/what-is-istio/
[^8]: https://linkerd.io/2018/09/18/announcing-linkerd-2-0/
[^9]: https://kinvolk.io/blog/2019/05/performance-benchmark-analysis-of-istio-and-linkerd/
[^10]: Here meaning cost of resources and performance impact.
[^11]: This is stretching the upper limits of Linkerd's acceptable response times.
